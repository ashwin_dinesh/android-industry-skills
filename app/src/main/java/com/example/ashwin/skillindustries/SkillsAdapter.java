package com.example.ashwin.skillindustries;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.ashwin.skillindustries.db.Skill;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ashwin on 27/11/17.
 */

public class SkillsAdapter extends RecyclerView.Adapter<SkillsAdapter.SkillViewHolder>  {

    public interface SkillSelectionListener {
        void onSkillSelected(Skill skill, boolean selected);
        void onSkillDeleted(Skill skill);
    }

    private List<Skill> mSkillsList;
    private SkillSelectionListener mSkillSelecionListener;

    public SkillsAdapter(ArrayList<Skill> skillsList, SkillSelectionListener skillSelectionListener) {
        mSkillsList = skillsList;
        mSkillSelecionListener = skillSelectionListener;
    }

    public class SkillViewHolder extends RecyclerView.ViewHolder {
        public int position;
        public CheckBox mCheckBox;
        public TextView mSkillTextView;
        public ImageView mDeleteImageView;

        public SkillViewHolder(View view) {
            super(view);
            mCheckBox = (CheckBox) view.findViewById(R.id.checkBox);
            mCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                    mSkillSelecionListener.onSkillSelected(mSkillsList.get(position), b);
                }
            });

            mSkillTextView = (TextView) view.findViewById(R.id.skillTextView);

            mDeleteImageView = (ImageView) view.findViewById(R.id.deleteImageView);
            mDeleteImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mSkillSelecionListener.onSkillDeleted(mSkillsList.get(position));
                }
            });
        }
    }

    @Override
    public SkillsAdapter.SkillViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_skill, parent, false);

        return new SkillViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SkillsAdapter.SkillViewHolder holder, int position) {
        Skill skill = mSkillsList.get(position);

        // Position
        holder.position = position;

        // Checkbox
        (holder.mCheckBox).setChecked(skill.getSelected());

        // Skill
        (holder.mSkillTextView).setText(skill.childSkill);
    }

    @Override
    public int getItemCount() {
        return mSkillsList.size();
    }

    public void setFilter(List<Skill> skillsList) {
        mSkillsList = new ArrayList<>();
        mSkillsList.addAll(skillsList);
        notifyDataSetChanged();
    }
}
