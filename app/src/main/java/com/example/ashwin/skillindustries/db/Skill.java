package com.example.ashwin.skillindustries.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.util.Log;

/**
 * Created by ashwin on 27/11/17.
 */

@Entity(tableName = "skills")
public class Skill {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    public Long id;

    @ColumnInfo(name = "industry")
    public String industry;

    @ColumnInfo(name = "parent_skill")
    public String parentSkill;

    @ColumnInfo(name = "child_skill")
    public String childSkill;

    @Ignore
    private boolean selected;

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public boolean getSelected() {
        return selected;
    }

    @Override
    public boolean equals(Object obj) {
        Skill skill = (Skill) obj;
        return (this.industry.equalsIgnoreCase(skill.industry)
                && this.parentSkill.equalsIgnoreCase(skill.parentSkill)
                && this.childSkill.equalsIgnoreCase(skill.childSkill) );
    }
}
