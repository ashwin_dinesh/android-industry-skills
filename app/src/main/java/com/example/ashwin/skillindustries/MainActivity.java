package com.example.ashwin.skillindustries;

import android.app.SearchManager;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.example.ashwin.skillindustries.db.Skill;
import com.example.ashwin.skillindustries.db.SkillDatabase;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener, SkillsAdapter.SkillSelectionListener {

    private SkillDatabase mDatabase;
    private ArrayList<Skill> mSkillsList;
    private List<Skill> mFilteredSkillsList = new ArrayList<>();
    private List<Skill> mSelectedSkillsList = new ArrayList<>();
    private boolean mFilter = false;

    private MenuItem mSearchItem, mAddItem;
    private SearchView mSearchView;
    private TextView mTextView;
    private RecyclerView mRecyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private SkillsAdapter mAdapter;
    private Button mSelectButton;
    private ScrollView mScrollView;
    private TableLayout mTableLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initDatabase();
        initViews();
    }

    private void initDatabase() {
        //mDatabase = SkillDatabase.getInMemoryDatabase(getApplicationContext());
        mDatabase = Room.databaseBuilder(getApplicationContext(), SkillDatabase.class, "skills-db")
                .allowMainThreadQueries()
                .build();
    }

    private void initViews() {
        mTextView = (TextView) findViewById(R.id.textView);

        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        mLayoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(mLayoutManager);
        mAdapter = new SkillsAdapter(mSkillsList, this);
        mRecyclerView.setAdapter(mAdapter);

        mSelectButton = (Button) findViewById(R.id.selectButton);
        mSelectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if ((mSelectButton.getText().toString()).equalsIgnoreCase("DONE")) {
                    onDoneClicked();
                } else {
                    onSelectClicked();
                }
            }
        });

        mScrollView = (ScrollView) findViewById(R.id.scrollView);
        mScrollView.setVisibility(View.GONE);
        mTableLayout = (TableLayout) findViewById(R.id.tableLayout);
        mTableLayout.setVisibility(View.GONE);
    }

    private void initData() {
        List<Skill> skills = mDatabase.skillModel().loadAllSkills();
        mSkillsList = new ArrayList<>(skills);
        updateSkillsList(mSkillsList);
    }

    @Override
    protected void onStart() {
        super.onStart();
        if ( (mSelectButton.getText().toString()).equalsIgnoreCase("DONE") ) {
            // Do nothing
        } else {
            initData();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);

        mSearchItem = menu.findItem(R.id.action_search);
        mAddItem = menu.findItem(R.id.action_add);

        mSearchView = (SearchView) MenuItemCompat.getActionView(mSearchItem);
        mSearchView.setOnQueryTextListener(this);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mSearchView.setSearchableInfo(searchManager.getSearchableInfo( getComponentName() ));
        mSearchView.setIconifiedByDefault(true);
        mSearchView.setQueryRefinementEnabled(true);
        mSearchView.setSubmitButtonEnabled(true);

        mSearchView.addOnAttachStateChangeListener(new View.OnAttachStateChangeListener()
        {
            @Override
            public void onViewAttachedToWindow(View arg0) {
                // search opened
            }

            @Override
            public void onViewDetachedFromWindow(View arg0) {
                if ( !(mSelectButton.getText().toString()).equalsIgnoreCase("DONE") ) {
                    updateSkillsList(mSkillsList);
                }
            }
        });

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                showAddSkillDialog();
                break;
        }

        return true;
    }

    private void showMenuItem(boolean show) {
        mSearchItem.setVisible(show);
        mAddItem.setVisible(show);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        String query = intent.getStringExtra(SearchManager.QUERY);
        filter(mSkillsList, query);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        filter(mSkillsList, newText);
        return true;
    }

    private void filter(List<Skill> skills, String query) {
        if (query != null && !(query.trim()).equals("")) {
            query = query.toLowerCase();
            mSelectedSkillsList = new ArrayList<>();
            mFilteredSkillsList = new ArrayList<>();
            for (Skill skill : skills) {
                final String childSkill = skill.childSkill.toLowerCase();
                final String parentSkill = skill.parentSkill.toLowerCase();
                final String industry = skill.industry.toLowerCase();
                if (childSkill.contains(query)
                        || parentSkill.contains(query)
                        || industry.contains(query)) {
                    mFilteredSkillsList.add(skill);
                }
            }
            mAdapter.setFilter(mFilteredSkillsList);
        }
        mFilter = true;
    }

    private void updateSkillsList(ArrayList<Skill> skillsList) {
        if (skillsList == null || skillsList.isEmpty()) {
            mTextView.setText("No skills");
            mTextView.setVisibility(View.VISIBLE);
            mRecyclerView.setVisibility(View.GONE);
            return;
        }
        mRecyclerView.setVisibility(View.VISIBLE);
        mAdapter = new SkillsAdapter(skillsList, this);
        mRecyclerView.setAdapter(mAdapter);
        mTextView.setVisibility(View.GONE);
    }

    @Override
    public void onSkillSelected(Skill skill, boolean selected) {
        if (selected) {
            if (!mSelectedSkillsList.contains(skill)) {
                mSelectedSkillsList.add(skill);
            }
        } else {
            mSelectedSkillsList.remove(skill);
        }
    }

    @Override
    public void onSkillDeleted(final Skill skill) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Confirm");
        dialogBuilder.setMessage("Are you sure to delete skill permanently?");
        dialogBuilder.setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mSkillsList.remove(skill);
                mDatabase.skillModel().deleteSkill(skill);
                updateSkillsList(mSkillsList);
            }
        });
        dialogBuilder.setNegativeButton(R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Dismiss
            }
        });

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    private void showAddSkillDialog() {
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.layout_add_skill, null);

        final EditText industryEditText = (EditText) dialogView.findViewById(R.id.industryEditText);
        final EditText parentSkillEditText = (EditText) dialogView.findViewById(R.id.parentSkillEditText);
        final EditText childSkillEditText = (EditText) dialogView.findViewById(R.id.childSkillEditText);

        final AlertDialog dialog = new AlertDialog.Builder(this)
                .setView(dialogView)
                .setTitle("Add Skill")
                .setPositiveButton(R.string.add, null)
                .setNegativeButton(android.R.string.cancel, null)
                .create();

        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(final DialogInterface dialog) {
                Button button = ((AlertDialog) dialog).getButton(AlertDialog.BUTTON_POSITIVE);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        String industry = industryEditText.getText().toString();
                        String parentSkill = parentSkillEditText.getText().toString();
                        String childSkill = childSkillEditText.getText().toString();
                        if (isEmpty(industry) || isEmpty(parentSkill) || isEmpty(childSkill)) {
                            Toast.makeText(getApplicationContext(), "Please fill all details", Toast.LENGTH_SHORT).show();
                        } else {
                            Skill skill = new Skill();
                            skill.industry = industry;
                            skill.parentSkill = parentSkill;
                            skill.childSkill = childSkill;

                            if (!mSkillsList.contains(skill)) {
                                mDatabase.skillModel().insertSkill(skill);
                                mSkillsList.add(skill);
                                updateSkillsList(mSkillsList);
                            }

                            dialog.dismiss();
                        }

                    }
                });
            }
        });

        dialog.show();
    }

    private boolean isEmpty(String str) {
        if (str == null) {
            return true;
        }

        str = str.trim();

        if (str.length() == 0) {
            return true;
        }

        return false;
    }

    private void onSelectClicked() {
        showMenuItem(false);
        mRecyclerView.setVisibility(View.GONE);
        mFilteredSkillsList = new ArrayList<>();
        mFilter = false;
        mSelectButton.setText("DONE");
        mTableLayout.removeAllViews();

        if (!mSelectedSkillsList.isEmpty()) {
            mTextView.setVisibility(View.GONE);
            mScrollView.setVisibility(View.VISIBLE);
            mTableLayout.setVisibility(View.VISIBLE);

            TableRow headerRow = new TableRow(this);

            TextView industryHeader = new TextView(this);
            industryHeader.setText("Industry\t\t");
            industryHeader.setPadding(5, 0, 5, 0);
            industryHeader.setTypeface(Typeface.DEFAULT_BOLD);
            headerRow.addView(industryHeader);

            TextView parentSkillHeader = new TextView(this);
            parentSkillHeader.setText("Parent Skill\t\t");
            parentSkillHeader.setPadding(5, 0, 5, 0);
            parentSkillHeader.setTypeface(Typeface.DEFAULT_BOLD);
            headerRow.addView(parentSkillHeader);

            TextView childSkillHeader = new TextView(this);
            childSkillHeader.setText("Child Skill\n");
            childSkillHeader.setPadding(5, 0, 5, 0);
            childSkillHeader.setTypeface(Typeface.DEFAULT_BOLD);
            headerRow.addView(childSkillHeader);

            mTableLayout.addView(headerRow);

            for (Skill skill : mSelectedSkillsList) {

                TableRow row = new TableRow(MainActivity.this);

                TextView industryTextView = new TextView(MainActivity.this);
                industryTextView.setText(skill.industry +"\t\t");
                industryTextView.setPadding(5, 0, 5, 0);
                industryTextView.setWidth(0);

                TextView parentSkillTextView = new TextView(MainActivity.this);
                parentSkillTextView.setText(skill.parentSkill + "\t\t");
                parentSkillTextView.setPadding(5, 0, 5, 0);
                parentSkillTextView.setWidth(0);

                TextView childSkillTextView = new TextView(MainActivity.this);
                childSkillTextView.setText(skill.childSkill + "\n");
                childSkillTextView.setPadding(5, 0, 5, 0);
                childSkillTextView.setWidth(0);

                row.addView(industryTextView);
                row.addView(parentSkillTextView);
                row.addView(childSkillTextView);

                mTableLayout.addView(row);
            }
        } else {
            mTextView.setVisibility(View.VISIBLE);
            mTextView.setText("Nothing selected");
        }
    }

    private void onDoneClicked() {
        showMenuItem(true);
        mFilter = false;
        mSelectButton.setText("SELECT");
        mSelectedSkillsList = new ArrayList<>();
        mScrollView.setVisibility(View.GONE);
        mTableLayout.setVisibility(View.GONE);
        updateSkillsList(mSkillsList);
    }

    @Override
    public void onBackPressed() {
        if ( (mSelectButton.getText().toString()).equalsIgnoreCase("DONE") ) {
            onDoneClicked();
        } else {
            super.onBackPressed();
        }
    }
}
