package com.example.ashwin.skillindustries.db;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.ArrayList;
import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.IGNORE;

/**
 * Created by ashwin on 27/11/17.
 */

@Dao
public interface SkillDao {

    @Query("select * from skills")
    List<Skill> loadAllSkills();

    @Query("SELECT * FROM skills WHERE child_skill LIKE '%' || :childSkill || '%'")
    List<Skill> loadSkills(String childSkill);

    @Insert(onConflict = IGNORE)
    void insertSkill(Skill skill);

    @Delete
    void deleteSkill(Skill skill);

    @Query("DELETE FROM skills")
    void deleteAll();

}
